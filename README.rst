VideoBrainz Schema
==================

Python package specifying the schema for the VideoBrainz database.

Installation
------------

Download the package and run setup.py.
