# -*- coding: utf-8 -*-

# Copyright (c) 2014 Ben Ockmore

from .resource import Resource, Base
from .derived import Entity, Event, Work, Production, Series, Edition
from .triples import (EntityTriple, EventTriple, WorkTriple, ProductionTriple,
                      SeriesTriple, EditionTriple)


def create_tables(database_uri):
    from sqlalchemy import create_engine
    engine = create_engine(database_uri)
    Base.metadata.create_all(engine)
