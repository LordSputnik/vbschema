# -*- coding: utf-8 -*-

# Copyright (c) 2014 Ben Ockmore

from sqlalchemy import Column, DateTime, UnicodeText, Unicode, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from .resource import Resource, Base


class Entity(Resource):
    __tablename__ = 'entity'
    __table_args__ = {'schema': 'videobrainz'}

    gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.resource.gid'),
                 primary_key=True)

    begin_event_gid = Column(UUID)
    end_event_gid = Column(UUID)

    __mapper_args__ = {
        'polymorphic_identity': 'ent',
    }


class Event(Resource):
    __tablename__ = 'event'
    __table_args__ = {'schema': 'videobrainz'}

    gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.resource.gid'),
                 primary_key=True)

    start = Column(DateTime)
    end = Column(DateTime)

    location = Column(UnicodeText)

    short_desc = Column(Unicode(length=80))

    main_desc = Column(UnicodeText)

    __mapper_args__ = {
        'polymorphic_identity': 'eve',
    }


class Work(Resource):
    __tablename__ = 'work'
    __table_args__ = {'schema': 'videobrainz'}

    gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.resource.gid'),
                 primary_key=True)

    name = Column(UnicodeText)

    __mapper_args__ = {
        'polymorphic_identity': 'wor',
    }


class Production(Resource):
    __tablename__ = 'production'
    __table_args__ = {'schema': 'videobrainz'}

    gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.resource.gid'),
                 primary_key=True)

    name = Column(UnicodeText)

    __mapper_args__ = {
        'polymorphic_identity': 'pro',
    }


class Series(Resource):
    __tablename__ = 'series'
    __table_args__ = {'schema': 'videobrainz'}

    gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.resource.gid'),
                 primary_key=True)

    name = Column(UnicodeText)

    __mapper_args__ = {
        'polymorphic_identity': 'ser',
    }


class Edition(Resource):
    __tablename__ = 'edition'
    __table_args__ = {'schema': 'videobrainz'}

    gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.resource.gid'),
                 primary_key=True)

    name = Column(UnicodeText)

    __mapper_args__ = {
        'polymorphic_identity': 'edi',
    }
