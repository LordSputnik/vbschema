# -*- coding: utf-8 -*-

# Copyright (c) 2014 Ben Ockmore

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, UnicodeText
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.sql import text

import uuid

Base = declarative_base()


class Resource(Base):
    __tablename__ = 'resource'
    __table_args__ = {'schema': 'videobrainz'}

    gid = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)

    revision = Column(Integer, nullable=False, server_default=text('1'))

    edits_pending = Column(Integer, nullable=False, server_default=text('0'))

    last_updated = Column(DateTime, nullable=False,
                          server_default=text('(now() AT TIME ZONE \'utc\')'))

    comment = Column(UnicodeText, nullable=False, server_default='')

    type = Column(String(3))

    __mapper_args__ = {
        'polymorphic_identity': 'res',
        'polymorphic_on': type
    }
