# -*- coding: utf-8 -*-

# Copyright (c) 2014 Ben Ockmore

from .resource import Base
from sqlalchemy import Column, Integer, ForeignKey, UnicodeText
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship, backref
from sqlalchemy.schema import UniqueConstraint


class Predicate(Base):
    __tablename__ = 'predicate'
    __table_args__ = {'schema': 'videobrainz'}

    id = Column(Integer, primary_key=True)
    s_o_text = Column(UnicodeText, nullable=False)
    o_s_text = Column(UnicodeText, nullable=False)


class EntityTriple(Base):
    __tablename__ = 'entity_triple'
    __table_args__ = {'schema': 'videobrainz'}

    id = Column(Integer, primary_key=True)

    entity_gid = Column(UUID(as_uuid=True),
                        ForeignKey('videobrainz.entity.gid'), nullable=False)

    predicate_id = Column(
        Integer, ForeignKey('videobrainz.predicate.id'), nullable=False
    )

    other_gid = Column(UUID(as_uuid=True),
                       ForeignKey('videobrainz.resource.gid'))

    other_text = Column(UnicodeText)

    entity = relationship('Entity', backref='triples',
                          foreign_keys=[entity_gid])

    predicate = relationship('Predicate')

    other = relationship('Resource', backref='entity_triples',
                         foreign_keys=[other_gid])

    UniqueConstraint('entity_gid', 'predicate_id', 'other_gid')
    UniqueConstraint('entity_gid', 'predicate_id', 'other_text')


class EventTriple(Base):
    __tablename__ = 'event_triple'
    __table_args__ = {'schema': 'videobrainz'}

    id = Column(Integer, primary_key=True)
    event_gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.event.gid'),
                       nullable=False)

    predicate_id = Column(Integer, ForeignKey('videobrainz.predicate.id'),
                          nullable=False)

    other_gid = Column(UUID(as_uuid=True),
                       ForeignKey('videobrainz.resource.gid'))

    other_text = Column(UnicodeText)

    event = relationship('Event', backref='triples', foreign_keys=[event_gid])
    predicate = relationship('Predicate')

    other = relationship('Resource', backref='event_triples',
                         foreign_keys=[other_gid])

    UniqueConstraint('event_gid', 'predicate_id', 'other_gid')
    UniqueConstraint('event_gid', 'predicate_id', 'other_text')


class WorkTriple(Base):
    __tablename__ = 'work_triple'
    __table_args__ = {'schema': 'videobrainz'}

    id = Column(Integer, primary_key=True)
    work_gid = Column(UUID(as_uuid=True), ForeignKey('videobrainz.work.gid'),
                      nullable=False)
    predicate_id = Column(Integer, ForeignKey('videobrainz.predicate.id'),
                          nullable=False)
    other_gid = Column(UUID(as_uuid=True),
                       ForeignKey('videobrainz.resource.gid'))
    other_text = Column(UnicodeText)

    work = relationship('Work', backref='triples', foreign_keys=[work_gid])
    predicate = relationship('Predicate')

    other = relationship('Resource', backref='work_triples',
                         foreign_keys=[other_gid])

    UniqueConstraint('work_gid', 'predicate_id', 'other_gid')
    UniqueConstraint('work_gid', 'predicate_id', 'other_text')


class ProductionTriple(Base):
    __tablename__ = 'production_triple'
    __table_args__ = {'schema': 'videobrainz'}

    id = Column(Integer, primary_key=True)
    production_gid = Column(
        UUID(as_uuid=True), ForeignKey('videobrainz.production.gid'),
        nullable=False
    )

    predicate_id = Column(Integer, ForeignKey('videobrainz.predicate.id'),
                          nullable=False)

    other_gid = Column(UUID(as_uuid=True),
                       ForeignKey('videobrainz.resource.gid'))

    other_text = Column(UnicodeText)

    production = relationship('Production', backref='triples',
                              foreign_keys=[production_gid])

    predicate = relationship('Predicate')
    other = relationship('Resource', backref='production_triples',
                         foreign_keys=[other_gid])

    UniqueConstraint('production_gid', 'predicate_id', 'other_gid')
    UniqueConstraint('production_gid', 'predicate_id', 'other_text')


class SeriesTriple(Base):
    __tablename__ = 'series_triple'
    __table_args__ = {'schema': 'videobrainz'}

    id = Column(Integer, primary_key=True)

    series_gid = Column(UUID(as_uuid=True),
                        ForeignKey('videobrainz.series.gid'), nullable=False)

    predicate_id = Column(Integer, ForeignKey('videobrainz.predicate.id'),
                          nullable=False)

    other_gid = Column(UUID(as_uuid=True),
                       ForeignKey('videobrainz.resource.gid'))

    other_text = Column(UnicodeText)

    series = relationship('Series', backref='triples',
                          foreign_keys=[series_gid])

    predicate = relationship('Predicate')
    other = relationship('Resource', backref='series_triples',
                         foreign_keys=[other_gid])

    UniqueConstraint('series_gid', 'predicate_id', 'other_gid')
    UniqueConstraint('series_gid', 'predicate_id', 'other_text')


class EditionTriple(Base):
    __tablename__ = 'edition_triple'
    __table_args__ = {'schema': 'videobrainz'}

    id = Column(Integer, primary_key=True)
    edition_gid = Column(UUID(as_uuid=True),
                         ForeignKey('videobrainz.edition.gid'), nullable=False)

    predicate_id = Column(Integer, ForeignKey('videobrainz.predicate.id'),
                          nullable=False)

    other_gid = Column(UUID(as_uuid=True),
                       ForeignKey('videobrainz.resource.gid'))

    other_text = Column(UnicodeText)

    edition = relationship('Edition', backref='triples',
                           foreign_keys=[edition_gid])

    predicate = relationship('Predicate')

    other = relationship('Resource', backref='edition_triples',
                         foreign_keys=[other_gid])

    UniqueConstraint('edition_gid', 'predicate_id', 'other_gid')
    UniqueConstraint('edition_gid', 'predicate_id', 'other_text')
